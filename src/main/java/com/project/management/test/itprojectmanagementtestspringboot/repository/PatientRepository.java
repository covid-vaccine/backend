package com.project.management.test.itprojectmanagementtestspringboot.repository;

import com.project.management.test.itprojectmanagementtestspringboot.model.Patient;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Integer>{

}
