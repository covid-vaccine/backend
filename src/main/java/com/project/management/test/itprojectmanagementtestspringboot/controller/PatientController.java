package com.project.management.test.itprojectmanagementtestspringboot.controller;

import com.project.management.test.itprojectmanagementtestspringboot.exception.ResourceNotFoundException;
import com.project.management.test.itprojectmanagementtestspringboot.model.Patient;
import com.project.management.test.itprojectmanagementtestspringboot.repository.PatientRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/patient")
public class PatientController {

    @Autowired
    private PatientRepository patientRepository;

    @GetMapping("/patients")
    public List<Patient> getAllPatients() {
        return patientRepository.findAll();
    }

    @GetMapping("/patients/{id}")
    public ResponseEntity<Patient> getPatientById(@PathVariable(value = "id") Integer patientId)
        throws ResourceNotFoundException {
        Patient patient = patientRepository.findById(patientId).orElseThrow(()->
                new ResourceNotFoundException("Cannot find: " + patientId));

        return ResponseEntity.ok().body(patient);
    }

    @PostMapping("/patients/add")
    public Patient createPatient(@RequestBody Patient patient) {
        return patientRepository.save(patient);
    }

    @PutMapping("/patients/{id}")
    public ResponseEntity<Patient> updatePatient(@PathVariable(value = "id") Integer patientId,
                                                 @RequestBody Patient patientDetails)
        throws ResourceNotFoundException {
        Patient patient = patientRepository.findById(patientId).orElseThrow(() ->
                new ResourceNotFoundException("Cannot find: " + patientId));

        patient.setEmailId(patientDetails.getEmailId());
        patient.setLastName(patientDetails.getLastName());
        patient.setFirstName(patientDetails.getFirstName());

        final Patient updatedPatient = patientRepository.save(patient);

        return ResponseEntity.ok(updatedPatient);
    }

    @DeleteMapping("/patients/{id}")
    public Map<String, Boolean> deletePatient(@PathVariable(value = "id") Integer patientId)
        throws ResourceNotFoundException {
        Patient patient = patientRepository.findById(patientId).orElseThrow(() ->
                new ResourceNotFoundException("Cannot find: " + patientId));

        patientRepository.delete(patient);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", Boolean.TRUE);

        return response;
    }
}
