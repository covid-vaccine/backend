package com.project.management.test.itprojectmanagementtestspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItProjectManagementTestSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(ItProjectManagementTestSpringbootApplication.class, args);
	}

}
