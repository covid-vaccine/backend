package com.project.management.test.itprojectmanagementtestspringboot;

import com.project.management.test.itprojectmanagementtestspringboot.model.Patient;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ItProjectManagementTestSpringbootApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PatientControllerIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    private String getRootUrl() {
        String rootUrl = "http://localhost:";

        return rootUrl + port;
    }

    @Test
    public void contextLoads() {

    }

    @Test
    public void testGetAllPatients() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/patients",
                HttpMethod.GET, entity, String.class);
        Assert.assertNotNull(response.getBody());
    }

    @Test
    public void testGetPatientById() {
        Patient patient = restTemplate.getForObject(getRootUrl() + "/patients/1", Patient.class);
        System.out.println(patient.getFirstName());
        Assert.assertNotNull(patient);
    }

    @Test
    public void testCreatePatient() {
        Patient patient = new Patient();
        patient.setFirstName("John");
        patient.setLastName("Wick");
        patient.setEmailId("john.wick@gmail.com");

        ResponseEntity<Patient> postResponse = restTemplate.postForEntity(getRootUrl() + "/patients",
                patient, Patient.class);
        Assert.assertNotNull(postResponse);
        Assert.assertNotNull(postResponse.getBody());
    }

    @Test
    public void testUpdatePatient() {
        int id = 1;

        Patient patient = restTemplate.getForObject(getRootUrl() + "/patients/" + id, Patient.class);
        patient.setFirstName("Mark");
        patient.setLastName("Henry");
        restTemplate.put(getRootUrl() + "/patients/" + id, patient);
        Patient updatedPatient = restTemplate.getForObject(getRootUrl() + "/patients/" + id, Patient.class);
        Assert.assertNotNull(updatedPatient);
    }

    @Test
    public void testDeletePatient() {
        int id = 2;

        Patient patient = restTemplate.getForObject(getRootUrl() + "/patients/" + id, Patient.class);
        assertNotNull(patient);
        restTemplate.delete(getRootUrl() + "/patients/" + id);
        try {
            patient = restTemplate.getForObject(getRootUrl() + "/patients/" + id, Patient.class);
        } catch (final HttpClientErrorException e) {
            assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
        }
    }
}
